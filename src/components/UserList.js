import React from 'react'
import User from './User';
import Footer from './Footer';
import PropTypes from 'prop-types';
import List from 'material-ui/List';

class UserList extends React.Component {
// function UserList(ownProps, state) {

    renderUsers () {
        if (!this.props.data.loading) {

        //const mappedUsers = this.props.users.map((el, i) => el);
        //
        //mappedUsers.sort(function(a, b) {
        //    if (a.id > b.id) {
        //        return 1;
        //    }
        //    if (a.id < b.id) {
        //        return -1;
        //    }
        //    return 0;
        //});

            return (
                <List>
                    {this.props.users.map(user =>
                        <User
                            key={user.id}
                            {...user}
                            toggleUserAttendance={() => this.props.toggleUserAttendance(user.id, !user.attendingNextYear)}
                            onOpenUserDetail={() => this.props.onOpenUserDetail(user.id)}
                            //onEditUser={() => this.props.onEditUser(user.id)}
                            onRemoveUser={() => this.props.onRemoveUser(user.id)}
                            />

                    )}
                </List>
            );
        }

        return <div>Loading</div>
    }

    render (props) {
        return (
            <div>
                {this.renderUsers()}
            <Footer />
            </div>
        )
    }

}

UserList.propTypes = {
    users: PropTypes.arrayOf(
        PropTypes.shape({
            firstName: PropTypes.string.isRequired,
            lastName: PropTypes.string.isRequired,
            email: PropTypes.string.isRequired,
            charClass: PropTypes.string.isRequired,
            regNum: PropTypes.number.isRequired,
            attendingNextYear: PropTypes.bool
        }).isRequired
    ),
    //onEditUser: PropTypes.func,
    onRemoveUser: PropTypes.func,
    onOpenUserDetail: PropTypes.func.isRequired,
    toggleUserAttendance: PropTypes.func.isRequired,
}

export default UserList;