import React from 'react'
import PropTypes from 'prop-types';
import _ from 'lodash';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import Avatar from 'material-ui/Avatar';
import deepOrange from 'material-ui/colors/deepOrange';
import List, { ListItem, ListItemText } from 'material-ui/List';

const styles = theme => ({

    paper: {
        color: theme.palette.text.secondary,
    },
    orangeAvatar: {
        margin: 10,
        color: '#fff',
        backgroundColor: deepOrange[500],
    },
});

const UserProfile = (props) => {
    const classes = props.classes
    if (!props.data.loading) {

        const user = _.get(props.data, 'users[0]');
        return (
            <Grid container spacing={8} justify='center' align='center'>
                <Grid item xs={8}>
                    <Paper className={classes.paper}>
                        <Avatar className={classes.orangeAvatar}>{user.firstName.substr(0,1)}</Avatar>
                        <h2>{user.firstName} {user.lastName}</h2>
                    </Paper>
                </Grid>
                <Grid item xs={8} className="profileDetails">
                    <Paper className={classes.paper}>
                        <List>
                            <ListItem>
                                <ListItemText inset primary={user.id} />
                            </ListItem>
                            <ListItem>
                                <ListItemText inset primary={user.regNum} />
                            </ListItem>
                            <ListItem>
                                <ListItemText inset primary={user.email} />
                            </ListItem>
                            <ListItem>
                                <ListItemText inset primary={user.firstName} />
                            </ListItem>
                            <ListItem>
                                <ListItemText inset primary={user.lastName} />
                            </ListItem>
                            <ListItem>
                                <ListItemText inset primary={user.charClass} />
                            </ListItem>
                            <ListItem>
                                <ListItemText inset primary={user.xp} />
                            </ListItem>
                            <ListItem>
                                <ListItemText inset primary={user.feets} />
                            </ListItem>
                            <ListItem>
                                <ListItemText inset primary={user.posts} />
                            </ListItem>
                            <ListItem>
                                <ListItemText inset primary={user.perks} />
                            </ListItem>
                            <ListItem>
                                <ListItemText inset primary={user.customizations} />
                            </ListItem>
                            <ListItem>
                                <ListItemText inset primary={user.attendingNextYear} />
                            </ListItem>
                        </List>
                    </Paper>

            </Grid>
                {/*<span onClick={props.toggleUserAttendance}>(TOGGLE ATTENDANCE)</span>*/}
                {/*<span onClick={props.onRemoveUser}>[X]</span>*/}
            </Grid>
        )
    }

    return <div>Loading</div>

};

UserProfile.propTypes = {

    users: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            regNum: PropTypes.number.isRequired,
            email: PropTypes.string.isRequired,
            firstName: PropTypes.string.isRequired,
            lastName: PropTypes.string.isRequired,
            charClass: PropTypes.string.isRequired,
            xp: PropTypes.number.isRequired,
            feets: PropTypes.number.isRequired,
            posts: PropTypes.string.isRequired,
            perks: PropTypes.string.isRequired,
            customizations: PropTypes.string.isRequired,
            attendingNextYear: PropTypes.bool.isRequired
        }).isRequired
    ),
}
//export default UserProfile;
export default withStyles(styles)(UserProfile);