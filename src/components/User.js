import React from 'react'
import PropTypes from 'prop-types';
import { ListItem, ListItemIcon, ListItemText, ListItemSecondaryAction } from 'material-ui/List';
import CalendarPlusIcon from 'mdi-react/CalendarPlusIcon';
import CalendarRemoveIcon from 'mdi-react/CalendarRemoveIcon';
import AccountRemoveIcon from 'mdi-react/AccountRemoveIcon';

const User = (props, state) => {

    const userInfo = `#${props.id} [${props.charClass}] - ${props.firstName} ${props.lastName}`;

    const isUserAttendingNextYear = props.attendingNextYear
        ? <CalendarRemoveIcon onClick={props.toggleUserAttendance} />
        : <CalendarPlusIcon onClick={props.toggleUserAttendance} />;

    return (
        <ListItem style={{textDecoration: props.attendingNextYear ? 'none' : 'line-through'}}>
            <ListItemIcon>
                {isUserAttendingNextYear}
            </ListItemIcon>
            <ListItemText onClick={props.onOpenUserDetail} inset primary={userInfo} />
            <ListItemSecondaryAction>
                <AccountRemoveIcon onClick={props.onRemoveUser} />
            </ListItemSecondaryAction>
        </ListItem>
    );
};

User.propTypes = {
    toggleUserAttendance: PropTypes.func.isRequired,
    onOpenUserDetail: PropTypes.func.isRequired,
    onRemoveUser: PropTypes.func.isRequired,
    //onEditUser: PropTypes.func.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    charClass: PropTypes.string.isRequired,
    regNum: PropTypes.number.isRequired,
    attendingNextYear: PropTypes.bool
}

export default User;

