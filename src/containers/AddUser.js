import React from 'react'
//import {connect} from 'react-redux'
//import {addUser} from '../actions'
import { compose, gql, graphql } from 'react-apollo';
import UserForm from '../components/UserForm';

const AddChannel = (props) => {


    return (
        <UserForm

            {...props}
            info={{
                firstName: 'defaultfirstName',
                lastName: 'defaultlastName',
                email: 'defaultEmail@default.com',
                charClass: 'defaultcharClass',
                regNum: 0,
                xp: 0,
                feets: 0,
                attendingNextYear: false
            }}
        />

    );
};


const submitRepository = gql`
mutation addUser(
        $firstName: String!
        $lastName: String!
        $email: String!
        $charClass: String!
        $regNum: Int!
        $xp: Int!
        $feets: Int!
        $attendingNextYear: Boolean!
    ) {
    addUser(
    firstName: $firstName, 
    lastName: $lastName,
    email: $email,
    charClass: $charClass,
    regNum: $regNum,
    xp: $xp,
    feets: $feets,
    attendingNextYear: $attendingNextYear
    ) {
        firstName
        lastName
        email
        charClass        
        regNum
        xp
        feets
        attendingNextYear
    }
  }
  `; // Same query as above



const AddChannelWithData = compose(
    graphql(submitRepository, {
        props: ({ mutate }) => ({
            submit: (input) => {
                mutate({
                    variables: {
                        firstName: input.firstName.value,
                        lastName: input.lastName.value,
                        charClass: input.charClass.value,
                        email: input.email.value,
                        regNum: input.regNum.value,
                        xp: input.xp.value,
                        feets: input.feets.value,
                        attendingNextYear: input.attendingNextYear,
                    }
                })
            },
        }),
    })

)(AddChannel);

export default AddChannelWithData;