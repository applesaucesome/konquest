import React from 'react';
import PropTypes from 'prop-types';
import { Route, Link, Switch } from 'react-router-dom'
import asyncComponent from '../components/AsyncComponent';
import AppliedRoute from '../components/AppliedRoute';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import { withStyles } from 'material-ui/styles';

const AsyncHome = asyncComponent(() => import('./Home'));
const AsyncAbout = asyncComponent(() => import('./About'));
const AsyncAddUser = asyncComponent(() => import('./AddUser'));
const AsyncUserList = asyncComponent(() => import('./VisibleUserList'));
const AsyncUserDetail = asyncComponent(() => import('./UserDetail'));
const AsyncNotFound = asyncComponent(() => import('../components/NotFound'));

const styles = theme => ({
    root: {
        flexGrow: 1,
        marginTop: 30,
    },
    paper: {
        padding: 16,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
});

const App = ({ childProps }) => (
    <Grid container spacing={24} direction='column' justify='center' align='center'>

            <Grid item xs={12}>
                <Paper>
                <header>
                    <Link to="/">Home</Link>
                    <Link to="http://konquest.wikia.com/wiki/Kon.Quest_Wiki" target="_blank">Wiki</Link>
                    <Link to="/about-us">About</Link>
                    <Link to="/add-users">Add Users</Link>
                    <Link to="/list-users">List Users</Link>
                    <Link to="/user/1">User Detail</Link>
                </header>
                </Paper>
            </Grid>

            <Grid item xs={12}>
                <Paper>
                <Switch>
                    <AppliedRoute path="/" exact component={AsyncHome} props={childProps} />
                    {/* TODO - replace <Route /> with <AuthenticatedRoute /> // <UnauthenticatedRoute />
                     http://serverless-stack.com/chapters/code-splitting-in-create-react-app.html
                     http://serverless-stack.com/chapters/add-the-user-token-to-the-state.html
                     */}
                    <AppliedRoute path="/about-us" exact component={AsyncAbout} props={childProps} />
                    <AppliedRoute path="/add-users" exact component={AsyncAddUser} props={childProps} />
                    <AppliedRoute path="/list-users" exact component={AsyncUserList} props={childProps} />
                    <AppliedRoute path="/user/:id" exact component={AsyncUserDetail} props={childProps} />
                    <Route component={AsyncNotFound} />
                </Switch>
                </Paper>
            </Grid>
    </Grid>
);

App.propTypes = {
    classes: PropTypes.object.isRequired,
};

//export default App
export default withStyles(styles)(App);